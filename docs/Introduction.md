﻿# PhyTA - Phylogenetic Tree Analytics

**PhyTA** is a tool to analize phylogenetic trees. Providing a unique software to process and compare phylogenetic trees using several comparison metrics and visualizing formats with the ability to analize the similarities or dissimilarities between the given trees.

You can access PhyTA using the following link: [http://cloud131.ncg.ingrid.pt/home](http://cloud131.ncg.ingrid.pt/home "http://cloud131.ncg.ingrid.pt/home")

![Overview of PhyTA System](_Images/arch.png)

Figure 1.1: Overview of PhyTA arquitecture.

## PhyTA Team

- [Inês de Castro](https://github.com/inesgc), ADEETC, ISEL, Instituto Politécnico de Lisboa

- [Rafael Pereira](https://github.com/rafaelpereira94), ADEETC, ISEL, Instituto Politécnico de Lisboa

- [Cátia Vaz](http://pwp.net.ipl.pt/cc.isel/cvaz/), INESC-ID / ADEETC, ISEL, Instituto Politécnico de Lisboa