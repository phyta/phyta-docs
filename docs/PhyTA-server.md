# PhyTA Server

The PhyTA Server is the business layer of PhyTA. It provides a way to compare phylogenetic trees using several metrics and returning the results in JSON. 

## Command Line

We offer a simple command line interface that is available [on node.js](https://www.npmjs.com/package/phyta-cli) or in the [github](https://www.github.com/RafaelPereira94/phyta-cli), that allows the user to process trees with the metrics and parsers available, returning the results in JSON file. 

**Options:**

* **`-h, --help `**
Shows the usage information.

* **`-V, --version`**
Shows the CLI version number.

* **`-l. --path <path>`**
Path to folder with files to be processed.

* **`-f, --trees <fileName>`** 
File name with the trees to be processed.

* **`-t, --types <fileName>`**
File name with tree types.

* **`-m, --metrics <fileName>`**
File name with metrics.

* **`-p,  --parser <parser>`**
Parser to be used on the trees.

* **`-o,  --output <fileName>`**
Name of the result files.

All command options are required, if any one of them is missing an exception will be thrown.

![Console Error](_Images/ErrorCli.JPG)
Figure 1.2: Error representation in console.

**Usage example:**

In order to run PhytA from command line you must specify, the metrics that will be used, the trees and their types. Each one in a different file. 
All the metrics, trees and tree types are separated by `$`character.

**Tree file:**

>trees.txt 

>(A,B,(C,D)E)F;$(A,B,(C,D)E)F;

**Tree types file:**

>types.txt

>rooted$rooted

**Metrics file:**

>metrics.txt

>robinsonFoulds$robinsonFouldsLength

Using the files specified above, we can run CLI using those commands:

If cloned from github repository:

> node cliApp.js --path C:/phyta/ --trees trees.txt --types types.txt --metrics metrics.txt --parser newick --output results

If you installed the module globally from npm:

> phyta-cli --path C:/phyta/ --trees trees.txt --types types.txt --metrics metrics.txt --parser newick --output results

**Results:**

The next code block shows us an example of the comparing results that each tree got with each other 
in the JSON output file.

```
{
  "trees": [
    {
      "id": 0,
      "format": "(A,B,(C,D)E)F;",
      "type": "rooted",
      "tree": {
        "elements": [
          {
            "id": 0,
            "parent": null,
            "children": [
              1,
              2,
              3
            ]
          },
          {
            "id": 1,
            "parent": 0,
            "children": []
          },
          {
            "id": 2,
            "parent": 0,
            "children": []
          },
          {
            "id": 3,
            "parent": 0,
            "children": [
              4,
              5
            ]
          },
          {
            "id": 4,
            "parent": 3,
            "children": []
          },
          {
            "id": 5,
            "parent": 3,
            "children": []
          }
        ],
        "info": {
          "0": {
            "data": "F",
            "length": 0
          },
          "1": {
            "data": "A",
            "length": 0
          },
          "2": {
            "data": "B",
            "length": 0
          },
          "3": {
            "data": "E",
            "length": 0
          },
          "4": {
            "data": "C",
            "length": 0
          },
          "5": {
            "data": "D",
            "length": 0
          }
        },
        "clusters": {
          "clusters": {
            "length": 6,
            "items": {
              "0": [
                1,
                2,
                4,
                5
              ],
              "1": [
                1
              ],
              "2": [
                2
              ],
              "3": [
                4,
                5
              ],
              "4": [
                4
              ],
              "5": [
                5
              ]
            }
          },
          "ids": [
            1,
            0,
            2,
            4,
            3,
            5
          ]
        }
      }
    },
    {
      "id": 1,
      "format": "(A,B,(C,D)E)F;",
      "type": "rooted",
      "tree": {
        "elements": [
          {
            "id": 6,
            "parent": null,
            "children": [
              7,
              8,
              9
            ]
          },
          {
            "id": 7,
            "parent": 6,
            "children": []
          },
          {
            "id": 8,
            "parent": 6,
            "children": []
          },
          {
            "id": 9,
            "parent": 6,
            "children": [
              10,
              11
            ]
          },
          {
            "id": 10,
            "parent": 9,
            "children": []
          },
          {
            "id": 11,
            "parent": 9,
            "children": []
          }
        ],
        "info": {
          "6": {
            "data": "F",
            "length": 0
          },
          "7": {
            "data": "A",
            "length": 0
          },
          "8": {
            "data": "B",
            "length": 0
          },
          "9": {
            "data": "E",
            "length": 0
          },
          "10": {
            "data": "C",
            "length": 0
          },
          "11": {
            "data": "D",
            "length": 0
          }
        },
        "clusters": {
          "clusters": {
            "length": 6,
            "items": {
              "6": [
                7,
                8,
                10,
                11
              ],
              "7": [
                7
              ],
              "8": [
                8
              ],
              "9": [
                10,
                11
              ],
              "10": [
                10
              ],
              "11": [
                11
              ]
            }
          },
          "ids": [
            7,
            6,
            8,
            10,
            9,
            11
          ]
        }
      }
    }
  ],
  "matrix": [
    [
      {
        "tree1": 0,
        "tree2": 0,
        "results": [
          {
            "metric": "rf",
            "result": {
              "result": 0,
              "diffsT1": [],
              "diffsT2": []
            }
          },
          {
            "metric": "rfl",
            "result": {
              "result": 0,
              "diffsT1": [],
              "diffsT2": []
            }
          }
        ]
      },
      {
        "tree1": 0,
        "tree2": 1,
        "results": [
          {
            "metric": "rf",
            "result": {
              "result": 0,
              "diffsT1": [],
              "diffsT2": []
            }
          },
          {
            "metric": "rfl",
            "result": {
              "result": 0,
              "diffsT1": [],
              "diffsT2": []
            }
          }
        ]
      }
    ],
    [
      {
        "tree1": 1,
        "tree2": 0,
        "results": [
          {
            "metric": "rf",
            "result": {
              "result": 0,
              "diffsT1": [],
              "diffsT2": []
            }
          },
          {
            "metric": "rfl",
            "result": {
              "result": 0,
              "diffsT1": [],
              "diffsT2": []
            }
          }
        ]
      },
      {
        "tree1": 1,
        "tree2": 1,
        "results": [
          {
            "metric": "rf",
            "result": {
              "result": 0,
              "diffsT1": [],
              "diffsT2": []
            }
          },
          {
            "metric": "rfl",
            "result": {
              "result": 0,
              "diffsT1": [],
              "diffsT2": []
            }
          }
        ]
      }
    ]
  ]
}
```
In the results we can see the information about each inserted tree,
 metric outcome and the difference between trees.

## Rest API

The PhyTA Server exports a Rest API to communicate with the PhyTA client, to allow others to use only the PhyTA server or to give others the option to develop their own client.

### Endpoints

#### Process Trees

----

  Parses the input trees and compares them returning a matrix with the trees
  and the result of the metrics.

* **URL**

  /process

* **Method:**

  `POST`
  
*  **URL Params**

   None

* **Data Params**

  **Required:**

  `trees=[string]` - Phylogenetic trees separated by &.
  `treeTypes=[string]` - Types of the trees separated by &.
  `metrics=[string]` - Metrics to process separated by &.
  `parser=[string]` - The parser to parse the trees.

  All tree types, metrics and parsers are specified in the configuration file.

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{"id": "59283b91b4d2413350b3ea0b"}`
 
 
* **Error Response:**

  * **Code:** 400 <br />
    **Content:** `{"error": "Please provide at least two trees!"}`


#### Check results

----

  This endpoint checks, if the results have been produced or not.If used 
  invalid jobId an error will be sent.

* **URL**

  /checkResults/:id


* **Method:**

  `GET`
  
  
*  **URL Params**

  **Required:**

  `id=[number]` - The id for the job.


* **Data Params**

  Node


* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 

   ```{
          "_id": "59bfe8b62e846f09942e896f",
          "trees": [
              {
                  "id": 0,
                  "format": "(A:0.1,B:0.2,(C:0.3,D:0.4)E:0.5)F;",
                  "type": "rooted",
                  "tree": {
                      "elements": [
                          {
                              "id": 0,
                              "parent": null,
                              "children": [
                                  1,
                                  2,
                                  3
                              ]
                          },
                          {
                              "id": 1,
                              "parent": 0,
                              "children": []
                          },
                          {
                              "id": 2,
                              "parent": 0,
                              "children": []
                          },
                          {
                              "id": 3,
                              "parent": 0,
                              "children": [
                                  4,
                                  5
                              ]
                          },
                          {
                              "id": 4,
                              "parent": 3,
                              "children": []
                          },
                          {
                              "id": 5,
                              "parent": 3,
                              "children": []
                          }
                      ],
                      "info": {
                          "0": {
                              "data": "F",
                              "length": 0
                          },
                          "1": {
                              "data": "A",
                              "length": 0.1
                          },
                          "2": {
                              "data": "B",
                              "length": 0.2
                          },
                          "3": {
                              "data": "E",
                              "length": 0.5
                          },
                          "4": {
                              "data": "C",
                              "length": 0.3
                          },
                          "5": {
                              "data": "D",
                              "length": 0.4
                          }
                      },
                      "clusters": {
                          "clusters": {
                              "length": 6,
                              "items": {
                                  "0": [
                                      1,
                                      2,
                                      4,
                                      5
                                  ],
                                  "1": [
                                      1
                                  ],
                                  "2": [
                                      2
                                  ],
                                  "3": [
                                      4,
                                      5
                                  ],
                                  "4": [
                                      4
                                  ],
                                  "5": [
                                      5
                                  ]
                              }
                          },
                          "ids": [
                              1,
                              0,
                              2,
                              4,
                              3,
                              5
                          ]
                      }
                  }
              },
              {
                  "id": 1,
                  "format": "(A:0.1,B:0.2,(C:0.3,D:0.4)E:0.5)F;",
                  "type": "rooted",
                  "tree": {
                      "elements": [
                          {
                              "id": 6,
                              "parent": null,
                              "children": [
                                  7,
                                  8,
                                  9
                              ]
                          },
                          {
                              "id": 7,
                              "parent": 6,
                              "children": []
                          },
                          {
                              "id": 8,
                              "parent": 6,
                              "children": []
                          },
                          {
                              "id": 9,
                              "parent": 6,
                              "children": [
                                  10,
                                  11
                              ]
                          },
                          {
                              "id": 10,
                              "parent": 9,
                              "children": []
                          },
                          {
                              "id": 11,
                              "parent": 9,
                              "children": []
                          }
                      ],
                      "info": {
                          "6": {
                              "data": "F",
                              "length": 0
                          },
                          "7": {
                              "data": "A",
                              "length": 0.1
                          },
                          "8": {
                              "data": "B",
                              "length": 0.2
                          },
                          "9": {
                              "data": "E",
                              "length": 0.5
                          },
                          "10": {
                              "data": "C",
                              "length": 0.3
                          },
                          "11": {
                              "data": "D",
                              "length": 0.4
                          }
                      },
                      "clusters": {
                          "clusters": {
                              "length": 6,
                              "items": {
                                  "6": [
                                      7,
                                      8,
                                      10,
                                      11
                                  ],
                                  "7": [
                                      7
                                  ],
                                  "8": [
                                      8
                                  ],
                                  "9": [
                                      10,
                                      11
                                  ],
                                  "10": [
                                      10
                                  ],
                                  "11": [
                                      11
                                  ]
                              }
                          },
                          "ids": [
                              7,
                              6,
                              8,
                              10,
                              9,
                              11
                          ]
                      }
                  }
              }
          ],
          "results": [
              [
                  {
                      "tree1": 0,
                      "tree2": 0,
                      "results": [
                          {
                              "metric": "rf",
                              "result": {
                                  "result": 0,
                                  "diffsT1": [],
                                  "diffsT2": []
                              }
                          }
                      ]
                  },
                  {
                      "tree1": 0,
                      "tree2": 1,
                      "results": [
                          {
                              "metric": "rf",
                              "result": {
                                  "result": 0,
                                  "diffsT1": [],
                                  "diffsT2": []
                              }
                          }
                      ]
                  }
              ],
              [
                  {
                      "tree1": 1,
                      "tree2": 0,
                      "results": [
                          {
                              "metric": "rf",
                              "result": {
                                  "result": 0,
                                  "diffsT1": [],
                                  "diffsT2": []
                              }
                          }
                      ]
                  },
                  {
                      "tree1": 1,
                      "tree2": 1,
                      "results": [
                          {
                              "metric": "rf",
                              "result": {
                                  "result": 0,
                                  "diffsT1": [],
                                  "diffsT2": []
                              }
                          }
                      ]
                  }
              ]
          ]
      }
     ```
	
  * **Error Response:**

  * **Code:** 400 <br />
    **Content:** `{"error": "The id entered does not exist"}`
    
    
#### ConfigFile
----

This endpoint send json configuration about the trees and 
the metrics they support.

* **URL**

  /configFile


* **Method:**

  `GET`
  
* **Success Response:**

  * **Code:** 200 <br />
    **Content:** 
    
    ```{
           "trees": [
               {
                   "id": "rooted",
                   "description": "Rooted tree",
                   "parser": "parseRootedTree",
                   "accepts": [
                       "rf",
                       "rfl",
                       "trip"
                   ]
               },
               {
                   "id": "unrooted",
                   "description": "Unrooted tree",
                   "parser": "parseUnrootedTree",
                   "accepts": [
                       "rf",
                       "rfl",
                       "quart"
                   ]
               },
               {
                   "id": "forest",
                   "description": "Tree forest",
                   "parser": "parseTreeForest",
                   "accepts": [
                       "rf",
                       "rfl",
                       "quart"
                   ]
               }
           ],
           "metrics": [
               {
                   "id": "robinsonFoulds",
                   "description": "Robinson Foulds Distance",
                   "metric": "rf"
               },
               {
                   "id": "robinsonFouldsLength",
                   "description": "Robinson Foulds with Lengths",
                   "metric": "rfl"
               },
               {
                   "id": "triplet",
                   "description": "Triplets distance",
                   "metric": "trip"
               },
               {
                   "id": "quartet",
                   "description": "Quartets distance",
                   "metric": "quart"
               }
           ],
           "parsers": [
               {
                   "id": "newick",
                   "description": "newick format",
                   "parser": "nwk"
               }
           ]
       }
       ```