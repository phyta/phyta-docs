# Developer

## Configuration file

**PhyTA** Server has a configuration file `config.json` that specifies all the tree types, parsers and metrics. It also specifies which tree types the metrics accept.

The configuration file is useful when adding new metrics, tree types and parsers as it provides a way to configure them.

For the next example we will be using the example of a configuration file in Figure 1.1.

    {
    "trees": [
        {
            "id": "rooted",
            "description": "Rooted tree",
            "parser": "parseRootedTree",
            "accepts": ["rf"]
        }
    ],
    "metrics": [
        {
            "id": "robinsonFoulds",
            "description": "Robinson Foulds Distance",
            "metric": "rf"
        }
    ],
    "parsers": [
        {
            "id": "newick",
            "description": "newick format",
            "parser": "nwk"
        }
    ]
    }
Figure 1.1 - Example of a configuration file

## Adding a new metric

If we wanted to add a new metric, for example, *Robinson and Foulds with Lengths* we would have to add a new metric object to the metrics array of the configuration file and specify, for each tree, if the metric can be used by the type.

The configuration file would be the one specified in Figure 1.2.

    {
    "trees": [
        {
            "id": "rooted",
            "description": "Rooted tree",
            "parser": "parseRootedTree",
            "accepts": ["rf", "rfl"]
        }
    ],
    "metrics": [
        {
            "id": "robinsonFoulds",
            "description": "Robinson Foulds Distance",
            "metric": "rf"
        },
        {
            "id": "robinsonFouldsLength",
            "description": "Robinson Foulds with Lengths",
            "metric": "rfl"
        }
    ],
    ...
    }
Figure 1.2 - Configuration file with the new metric.

The *id* property is the metric name that the user will use to make requests in the server.

Next we create a file with the metric implementation and add it to the *metrics* folder of the project. The name of the file must be the *metric* property in the new metric object added to the configuration file.

## Adding a new parser

Adding a new parser, for example *comma-separated values* - CSV, would result in adding a new parser object to the parsers array.

The configuration file would be the one specified in Figure 1.3.

    {
    ...,
    "parsers": [
        {
            "id": "newick",
            "description": "newick format",
            "parser": "nwk"
        },
        {
            "id": "csv",
            "description": "comma-separated values",
            "parser": "csv"
        }
    ]
    }
Figure 1.3 - Configuration file with the new parser.

The *id* property is the parser name the user will use to make requests to the server.

We also need to add a file with the parser code in the folder *parsers* of the project. The file must specify a way to parse each type of existent tree, not forgetting that it should only export one function. The name of the file must be the one specified in the *parser* property.

## Adding a new tree type

Adding a new tree type is a little bit tricky. It involves adding a new tree type object to the trees array of the configuration file, like in Figure 1.4.

    {
    "trees": [
        {
            "id": "rooted",
            "description": "Rooted tree",
            "parser": "parseRootedTree",
            "accepts": ["rf"]
        },
        {
            "id": "unrooted",
            "description": "Unrooted tree",
            "parser": "parseUnrootedTree",
            "accepts": ["rf"]
        }
    ],
    ...
    }
Figure 1.4 - Configuration file with the new tree type.

We also need to add a file with the tree implementation to the *model* folder.

This implementation must have the following requirements:
-  extend *Tree*;
- have a *getRepresentation()* function;
- have a *createElement()* function;
- add a new function in all the parsers that knows how to parse the tree type implemented, the function must be called like the *parser* property;

## Developing a new client

Because PhyTA is a modular solution it is possible to develop a new client that uses the PhyTA Server by comunicating via the *REST API*.
More information about the *REST API* can be read in the PhyTA Server documentation above.
