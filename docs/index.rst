.. PhyTA documentation master file.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PhyTA documentation!
====================================

`PhyTA` is a tool to analize phylogenetic trees. Providing a unique software to compare phylogenetic trees using several metrics and visualize the similarities or dissimilarities between them.

.. toctree::
   :maxdepth: 2

   Introduction.md
   PhyTA.md
   PhyTA-server.md
   PhyTA-client.md
   developer.md
   Features.md

